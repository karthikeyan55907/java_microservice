package com.advento.teacherms.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.advento.teacherms.dto.TeachermsDTO;
import com.advento.teacherms.service.TeachermsService;




@Service(value = "teachermsServiceImpl")
public class TeachermsServiceImpl implements TeachermsService {

	
	
	
	

	
		
	    @Override
		public SortedSet<TeachermsDTO> getTeacherms() {
	    	
	    	
	    	
	    	
	    	  SortedSet<TeachermsDTO> teachermsDTOs = new TreeSet<TeachermsDTO>();
	    		try {
	    			Class.forName("com.mysql.jdbc.Driver");

	    			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root",
	    					"Karthi@123");
	    			Statement stmt = conn.createStatement();
	    			ResultSet rs = stmt.executeQuery("select * from teacherms ");

	    			//int i = 0;
	    			while (rs.next()) {
	    				int id = rs.getInt("id");
	    				String name = rs.getString("name");
	    				int age = rs.getInt("age");
	    				String emailId = rs.getString("emailid");
	    				long salary = rs.getLong("salary");
	    				int height = rs.getInt("height");
	    				
	    				TeachermsDTO teachermsDTO = createStdObj(id, name, age, emailId, salary,height);
	    				
	    			    teachermsDTOs.add(teachermsDTO);

	    			}
	    		} catch (SQLException sqlEx) {
	    			sqlEx.printStackTrace();
	    		} catch (ClassNotFoundException e) {
	    			e.printStackTrace();
	    			
	    		}
	    		 return teachermsDTOs;
	    		 }
	    		
	    
	    
	    
	    
	    @Override
	    public Map<Integer, SortedSet<TeachermsDTO>> getAllTeacherHeight(int heightv) {
	        Map<Integer, SortedSet<TeachermsDTO>> mapTeacherms = new HashMap<>();
	        try {
	        	Class.forName("com.mysql.jdbc.Driver");

    			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root",
    					"Karthi@123");
	            // Create Statement
	            Statement stmt = conn.createStatement();
	            // Execute Query
	            ResultSet rs = stmt.executeQuery("select * from teacherms where height=" + heightv);
	            // iterate the Resultset
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");
	                int age = rs.getInt("age");
	                String emailId = rs.getString("emailid");
	                long salary = rs.getLong("salary");
	                int height = rs.getInt("height");
	               
	               
	                TeachermsDTO teacherDTO =  createStdObj(id, name, age, emailId, salary,height);
	                addTeacherMapH(mapTeacherms, teacherDTO);
	            }
	        } catch (SQLException sqlEx) {
	            sqlEx.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return mapTeacherms;
	    }
	public void addTeacherMapH(Map<Integer,SortedSet<TeachermsDTO>> mapTeacherms,TeachermsDTO teachermsDTO) {
	        int height = teachermsDTO.getHeight();
	        if (mapTeacherms.containsKey(height)) {
	            SortedSet<TeachermsDTO> newValList = mapTeacherms.get(height);
	            newValList.add(teachermsDTO);
	        }
	        else {
	            SortedSet<TeachermsDTO> studentList = new TreeSet<>();
	            studentList.add(teachermsDTO);
	            mapTeacherms.put(height, studentList);
	        }
	    }
	
	
	
	  @Override
	    public Map<Integer, SortedSet<TeachermsDTO>> getAllTeacherAge(int Age) {
	        Map<Integer, SortedSet<TeachermsDTO>> mapTeacherms = new HashMap<>();
	        try {
	        	Class.forName("com.mysql.jdbc.Driver");

  			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root",
  					"Karthi@123");
	            // Create Statement
	            Statement stmt = conn.createStatement();
	            // Execute Query
	            ResultSet rs = stmt.executeQuery("select * from teacherms where age=" + Age);
	            // iterate the Resultset
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String name = rs.getString("name");
	                int age = rs.getInt("age");
	                String emailId = rs.getString("emailid");
	                long salary = rs.getLong("salary");
	                int height = rs.getInt("height");
	                
	               
	                TeachermsDTO teacherDTO =  createStdObj(id, name, age, emailId, salary,height);
	                addTeacherMapA(mapTeacherms, teacherDTO);
	            }
	        } catch (SQLException sqlEx) {
	            sqlEx.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return mapTeacherms;
	    }
	public void addTeacherMapA(Map<Integer,SortedSet<TeachermsDTO>> mapTeacherms,TeachermsDTO teachermsDTO) {
	        int age = teachermsDTO.getAge();
	        if (mapTeacherms.containsKey(age)) {
	            SortedSet<TeachermsDTO> newValList = mapTeacherms.get(age);
	            newValList.add(teachermsDTO);
	        }
	        else {
	            SortedSet<TeachermsDTO> studentList = new TreeSet<>();
	            studentList.add(teachermsDTO);
	            mapTeacherms.put(age, studentList);
	        }
	    }
	    
	
	
	
	
	  private TeachermsDTO createStdObj(int id, String name, int age, String emailId, long salary, int height) {
	    	TeachermsDTO stdDTO = new TeachermsDTO();
			stdDTO.setId(id);
			stdDTO.setName(name);
			stdDTO.setAge(age);
			stdDTO.setEmailId(emailId);
			stdDTO.setSalary(salary);
			stdDTO.setHeight(height);
			

			return stdDTO;
		}
	}
	    
			
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    		
//	    		 <TeachermsDTO> teacherDtoSet = new HashSet<>();
//	    	        teacherDtoSet.addAll(teachermsDTOs);
//	    	        
//	    	        teachermsDTOs.clear();
//	    	        teachermsDTOs.addAll(teacherDtoSet);
//	    	        
	    	       
	    	

			
	  /*  @Override
		public TeachermsDTO getTeachersms(int stdId) {
			TeachermsDTO TeachermsDTO = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");

				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root",
						"Karthi@123");

				Statement stmt = conn.createStatement();

				ResultSet rs = stmt.executeQuery("select * from students where id=" + stdId);

				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					int age = rs.getInt("age");
					String emailId = rs.getString("emailid");
					long salary = rs.getLong("salary");
				
					int height = rs.getInt("height");
					
					TeachermsDTO TeachermsDTO1 = createStdObj(id, name, age, emailId, salary, height);
					TeachermsDTO = TeachermsDTO1;
				}

			} catch (SQLException sqlEx) {
				sqlEx.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return TeachermsDTO;
		}

		@Override
		public void createTeacherms(TeachermsDTO TeachermsDTO) {
			try {
				Class.forName("com.mysql.jdbc.Driver");

				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root",
						"Karthi@123");
				// Create Statement
				PreparedStatement pStmt = conn.prepareStatement(
						"insert into teacherms (name,age,emailid,salary,height) values (?,?,?,?,?)");
				pStmt.setString(1, TeachermsDTO.getName());

				pStmt.setInt(2, TeachermsDTO.getAge());
				pStmt.setString(3, TeachermsDTO.getEmail());

				pStmt.setLong(4, TeachermsDTO.getSalary());
				pStmt.setInt(5, TeachermsDTO.getHeight());

				// Execute Query
				boolean insertStatus = pStmt.execute();
				System.out.println("insertStatus--->" + insertStatus);
			} catch (SQLException sqlEx) {
				sqlEx.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void alterTeacherms(TeachermsDTO TeachermsDTO, int stdId) {
			try {

				Class.forName("com.mysql.jdbc.Driver");
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root","Karthi@123");
				String query = "update teacherms  set name=?,age=?, emailid=?, salary=?, height=? where id="+ stdId;

				PreparedStatement pStmt = conn.prepareStatement(query);
				pStmt.setString(1, TeachermsDTO.getName());
				pStmt.setInt(2, TeachermsDTO.getAge());
				pStmt.setString(3, TeachermsDTO.getEmail());
				pStmt.setLong(4, TeachermsDTO.getSalary());
				
				pStmt.setInt(5, TeachermsDTO.getHeight());
				boolean insertStatus = pStmt.execute();
				System.out.println("insertStatus--->" + insertStatus);
			} catch (SQLException sqlEx) {
				sqlEx.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void deleteTeacherms(int stdId) {
			  try {
		           
	        	  Class.forName("com.mysql.jdbc.Driver");
	              //Establish the connection
	              Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root", "Karthi@123");
	  				            //Create Statement
	            PreparedStatement pStmt = conn.prepareStatement("Delete from students  where id="+stdId);
//	            pStmt.setString(1, employeeDTO.getName());
//	            pStmt.setInt(2, employeeDTO.getAge());
//	            pStmt.setString(3, employeeDTO.getEmail());
//	            //Execute Query
	            boolean insertStatus = pStmt.execute();
	            System.out.println("insertStatus--->"+insertStatus);
//	            int i = 0;
	        } 
	        catch (SQLException sqlEx) {
	            sqlEx.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }

		}

		@Override
		public void patchTeacherms(TeachermsDTO TeachermsDTO, int stdId) {
			 try {
		            
	       	  Class.forName("com.mysql.jdbc.Driver");
	             //Establish the connection
	             Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentms", "root", "Karthi@123");
	 				           
	           TeachermsDTO patchTeachermsDTO = getTeacherms(stdId);
	           if (TeachermsDTO.getName() != null && !TeachermsDTO.getName().trim().equals("")) {
	               patchTeachermsDTO.setName(TeachermsDTO.getName());
	           }

	           if (TeachermsDTO.getAge() !=0) {
	               patchTeachermsDTO.setAge(TeachermsDTO.getAge());
	           }
	          
	           
	           if (TeachermsDTO.getEmail() != null && !TeachermsDTO.getEmail().trim().equals("")) {
	               patchTeachermsDTO.setEmail(TeachermsDTO.getEmail());
	           }
	           if (TeachermsDTO.getSalary() !=0) {
	               patchTeachermsDTO.setSalary(TeachermsDTO.getSalary());
	           }
	              
	               
	               if (TeachermsDTO.getHeight() !=0) {
	                   patchTeachermsDTO.setHeight(TeachermsDTO.getHeight());
	               }
	              
	               
	           //Update Query            
	           PreparedStatement pStmt = conn.prepareStatement("update teacherms set name=?, age=?,  emailid=?, salary=?, height=?  where id=?");
//	           pStmt.setString(1, patchTeachermsDTO.getName());
//	          
//	           pStmt.setString(2, patchTeachermsDTO.getEmail());
//	           pStmt.setInt(3, stdId);
	           
	           
	           
	           pStmt.setString(1,  patchTeachermsDTO.getName());
				pStmt.setInt(2,  patchTeachermsDTO.getAge());
				pStmt.setString(3,  patchTeachermsDTO.getEmail());
				pStmt.setLong(4,  patchTeachermsDTO.getSalary());
			
				pStmt.setInt(5,  patchTeachermsDTO.getHeight());
								 pStmt.setInt(6, stdId);

	           //Execute Query
	           int updStatus = pStmt.executeUpdate();
	           System.out.println("updateStatus--->"+updStatus);
	       } catch (SQLException sqlEx) {
	           sqlEx.printStackTrace();
	       } catch (ClassNotFoundException e) {
	           e.printStackTrace();
	       }
	       
		}
	    
	    */
	    
	    
	  
			
		
		

	
//        }
//        
//        if (mapStd.containsKey(std)) {
//
//            if (mapStd.get(std).containsKey(sec)) {
//                List<SamplemsDTO> newStuList = mapStd.get(std).get(sec);
//                newStuList.add(samplemsDTO);
//            } else {
//                Map<String, List<SamplemsDTO>> stdMap = mapStd.get(std);
//                Map<String, List<SamplemsDTO>> sectionMap = new HashMap<>();
//                List<SamplemsDTO> studentList = new ArrayList<SamplemsDTO>();
//                studentList.add(samplemsDTO);
//                stdMap.put(sec, studentList);
//            }
//        } else {
//            List<SamplemsDTO> studentList = new ArrayList<SamplemsDTO>();
//            studentList.add(samplemsDTO);
//            Map<String, List<SamplemsDTO>> sectionMap = new HashMap<>();
//            List<SamplemsDTO> students = new ArrayList<>();
//            students.add(samplemsDTO);
//            sectionMap.put(sec, students);
//            mapStd.put(std, sectionMap);
//
//        }
//	}
//	}		
//			
		

