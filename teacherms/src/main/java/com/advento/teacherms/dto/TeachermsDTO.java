package com.advento.teacherms.dto;

public class TeachermsDTO implements Comparable<TeachermsDTO> {
		

	
			private int id;
			
			private String name;
			
			private int age;
			
			private String emailId;
			
			private long salary;
			
			private int height;
			
			
			

			
			
			public TeachermsDTO(int id, String name, int age, String emailId, long salary,int height) {
			super();
				this.id = id;
				this.name = name;
				this.age = age;
				this.emailId = emailId;
				this.salary=salary;
				this.height=height;
			}

			

		

			public TeachermsDTO() {
			}





			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public int getAge() {
				return age;
			}

			public void setAge(int age) {
				this.age = age;
			}

			public String getEmailId() {
				return emailId;
			}

			public void setEmailId(String emailId) {
				this.emailId = emailId;
			}
			
			public long getSalary() {
				return salary;
			}

			public void setSalary(long salary) {
				this.salary = salary;
			}

			public int getHeight() {
				return height;
			}

			public void setHeight(int height) {
				this.height = height;
			}
			
			
			@Override
		    public int compareTo(TeachermsDTO teachermsDTO) {
		        int result = (this.emailId).compareTo(teachermsDTO.getEmailId());
		        System.out.println("result="+result);
		        return result;
		    }
			
			@Override
			public String toString() {
				return " SamplemsDTO [id=" + id + ", name=" + name + ", age=" + age + ", emailId=" + emailId + ",salary="+salary+",height="+height+"]";
			}
}
		
	