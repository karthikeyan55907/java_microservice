package com.advento.teacherms.controller;



	
	

import java.util.Map;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.advento.teacherms.dto.TeachermsDTO;
import com.advento.teacherms.service.TeachermsService;



@RestController
public class TeachermsController {

	@Autowired
	@Qualifier(value = "teachermsServiceImpl")
	private TeachermsService teachermsService;



	
	
	@GetMapping(value ="/teacherms")
	public ResponseEntity <SortedSet<TeachermsDTO>> getTeacherms(){
		SortedSet<TeachermsDTO> stdDTOs = teachermsService.getTeacherms();
		ResponseEntity<SortedSet<TeachermsDTO>> responseEntity = new ResponseEntity<SortedSet<TeachermsDTO>>(stdDTOs, HttpStatus.OK);
		return responseEntity;
		
	}
		@GetMapping(value = "/teachermsbyht/{heightv}")
	    public ResponseEntity < Map<Integer, SortedSet<TeachermsDTO>>> getAllTeacherHeight(@PathVariable(value = "heightv") int heightv) {
			 Map<Integer, SortedSet<TeachermsDTO>> studentDto = teachermsService. getAllTeacherHeight(heightv);
	        ResponseEntity< Map<Integer, SortedSet<TeachermsDTO>>> responseEntity = new ResponseEntity< Map<Integer, SortedSet<TeachermsDTO>>>(studentDto, HttpStatus.OK);
	        //LOGGER.info(responseEntity);
	        return responseEntity;
	    }
		@GetMapping(value = "/teachermsbyage/{age}")
	    public ResponseEntity < Map<Integer, SortedSet<TeachermsDTO>>> getAllTeacherAge(@PathVariable(value = "age") int Age) {
			 Map<Integer, SortedSet<TeachermsDTO>> studentDto = teachermsService. getAllTeacherAge(Age);
	        ResponseEntity< Map<Integer, SortedSet<TeachermsDTO>>> responseEntity = new ResponseEntity< Map<Integer, SortedSet<TeachermsDTO>>>(studentDto, HttpStatus.OK);
	        //LOGGER.info(responseEntity);
	        return responseEntity;
	    }
}
//	@GetMapping(value ="/Teacherms/{stdid1}")
//	public ResponseEntity<TeachermsDTO> getTeacherms(@PathVariable(value = "stdid1")int stdId){
//		TeachermsDTO stdDTOs = teachermsService.getTeachersms(stdId);
//		ResponseEntity<TeachermsDTO> responseEntity = new ResponseEntity<TeachermsDTO>(stdDTOs, HttpStatus.OK);
//		return responseEntity;
//	}
//	@PostMapping(value = "/Students")
//	public ResponseEntity<Void> createStudents(@RequestBody StudentDTO studentDTO) {
//		studentService.createStudents(studentDTO);
//		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.CREATED);
//		return responseEntity;
//	}
//	
//	@PutMapping(value = "/Students/{stdid2}")
//	public ResponseEntity<Void> alterEmployees(@RequestBody StudentDTO studentDTO,@PathVariable(value = "stdid2")int stdId) {
//		studentService.alterStudents(studentDTO, stdId);
//		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.OK);
//		return responseEntity;
//	
//	}
//	 @DeleteMapping(value  = "/Students/{stdid}")
//	    public ResponseEntity<Void> deleteEmployee(@PathVariable(value="stdid") int stdId) {
//		 studentService.deleteStudents(stdId);
//	        ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.OK);
//	        return responseEntity;
//	    }
//	 @PatchMapping(value  = "/Students/{stdid}")
//	    public ResponseEntity<Void> PatchEmployee(@RequestBody StudentDTO studentDTO,@PathVariable(value="stdid") int stdId) {
//		 studentService.patchStudents(studentDTO,stdId);
//	        ResponseEntity<Void> response = new ResponseEntity<Void>(HttpStatus.OK);
//	        return response;
//	    }
	

